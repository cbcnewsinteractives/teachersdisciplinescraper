import urllib
import csv
import re
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

baseURL = "https://www.bcteacherregulation.ca/ProfessionalConduct/SearchDisciplineOutcomes.aspx"

topcolumns = ['FirstName', 'lastName', 'date', 'caseCategory', 'caseType', 'caseLink']

MacFile = csv.writer(file('bcTeachersDisciplineWithCategories.csv','w'),dialect='excel')
MacFile.writerow(topcolumns)

selectMenu = {
	"1" : "Abuse of sick leave/other leave", 
	"2" : "Boundary violations", 
	"3" : "Competency", 
	"4" : "Criminal charge/conviction", 
	"5" : "Disability", 
	"6" : "Dishonesty", 
	"7" : "Duty to Regulator", 
	"8" : "Failure to report", 
	"9" : "Failure to supervise/ensure safety", 
	"10" : "Illegal substances", 
	"11" : "Inappropriate communication", 
	"12" : "Inappropriate conduct", 
	"13" : "Inappropriate physical contact", 
	"14" : "Inappropriate use of social media", 
	"16" : "Off-Duty conduct", 
	"15" : "Preliminary"
}

catNum = 1

driver = webdriver.Firefox()
time.sleep(1)
driver.get(baseURL)

# <select name="dpSearchTopic" id="dpSearchTopic" onchange="setSearchTopic(this, 17);" style="font-size:11px;width:260px;">
# 	<option selected="selected" value="0"></option>
# 	<option value="1">Abuse of sick leave/other leave</option>
# 	<option value="2">Boundary violations</option>
# 	<option value="3">Competency</option>
# 	<option value="4">Criminal charge/conviction</option>
# 	<option value="5">Disability</option>
# 	<option value="6">Dishonesty</option>
# 	<option value="7">Duty to Regulator</option>
# 	<option value="8">Failure to report</option>
# 	<option value="9">Failure to supervise/ensure safety</option>
# 	<option value="10">Illegal substances</option>
# 	<option value="11">Inappropriate communication </option>
# 	<option value="12">Inappropriate conduct</option>
# 	<option value="13">Inappropriate physical contact </option>
# 	<option value="14">Inappropriate use of social media</option>
# 	<option value="16">Off-Duty conduct</option>
# 	<option value="15">Preliminary</option>

# </select>

def maxSearchPages(driver):
	pageCount = 0
	sourceCode = driver.page_source
	if (re.search('Page <b>1</b> of <b>1</b>', sourceCode)):
		internalElem = driver.find_element_by_id('dgDisciplineDecisions')
		thisHTML = internalElem.get_attribute('innerHTML').encode('utf-8', errors='ignore')
		extractTable(thisHTML, catNum)
		return 0
	internalElem = driver.find_element_by_id('dgDisciplineDecisions')
	thisHTML = internalElem.get_attribute('innerHTML').encode('utf-8', errors='ignore')
	pagesToPickFrom = re.search('<td colspan="3">(.+?)</td>', thisHTML, re.S|re.DOTALL)
	pages = re.finditer('<a href="javascript(.+?)">(.+?)</a>', pagesToPickFrom.group(0))
	for every_page in pages:
		pageCount = pageCount + 1
	return pageCount + 1

def searchCategory(catNum):
	catText = selectMenu[str(catNum)]
	selectElement = driver.find_element_by_id('dpSearchTopic')
	for option in selectElement.find_elements_by_tag_name('option'):
		if option.text == catText:
			option.click() # select() in earlier versions of webdriver
			break
	submitButton = driver.find_element_by_id("imgbtnSearch")
	submitButton.click()
	pullTheseSearchResults(catNum)

def extractTable(tableHTML, catNum):
	# problem is in the regex - just go through more sparingly instead of taking the whole thing 
	for every_row in re.finditer('<tr(.+?)</tr>', tableHTML, re.S|re.DOTALL):
		try:
			date = re.search('<td align="center" valign="middle">(.+?)</td>', every_row.group(1))
			date = date.group(1)
			name = re.search('<td align="left" valign="middle">(.+?),<br ?/?>(.+?)</td>', every_row.group(1))
			firstName = name.group(2)
			lastName = name.group(1)
			caseLink = re.search('href="(.+?)"', every_row.group(1))
			caseLink = "https://www.bcteacherregulation.ca" + caseLink.group(1)
			caseType = re.search('<a (.+?)>(.+?)</a>', every_row.group(1))
			caseType = caseType.group(2)
			caseCategory = selectMenu[str(catNum)]
			rowData = [firstName, lastName, date, caseCategory, caseType, caseLink]
			MacFile.writerow(rowData)
			print rowData
		except: 
			pass

def pullTheseSearchResults(catNum):
	mostPages = maxSearchPages(driver)
	keepGoing = 2

	while keepGoing <= mostPages:		
		sourceCode = driver.page_source
		internalElem = driver.find_element_by_id('dgDisciplineDecisions')
		thisHTML = internalElem.get_attribute('innerHTML').encode('utf-8', errors='ignore')
		extractTable(thisHTML, catNum)
		if keepGoing == 21:
			driver.find_element_by_xpath(u'//a[text()=" [Next] "]').click()
			time.sleep(4)
			keepGoing = keepGoing + 1
		if driver.find_element_by_xpath(u'//a[text()="' + str(keepGoing) + '"]') and keepGoing != 21:
			nextButton = driver.find_element_by_xpath(u'//a[text()="' + str(keepGoing) + '"]')
			nextButton.click()
			time.sleep(2)
			keepGoing = keepGoing + 1


while catNum <= 15:
	searchCategory(catNum)
	catNum = catNum + 1

driver.quit()









