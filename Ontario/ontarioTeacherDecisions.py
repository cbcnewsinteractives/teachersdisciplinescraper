import csv
import urllib
import time
import re

topcolumns = ['ID', 'FirstName', 'LastName', 'DecisionDate', 'DecisionType', 'Committee', 'caseLink', 'teacherLink']

MacFile = csv.writer(file('ontarioTeacherDecisionsUpdated.csv','w'),dialect='excel')
MacFile.writerow(topcolumns)

baseURL = "https://www.oct.ca/public/complaints%20and%20discipline/decisions"
baseDecisionURL = "https://www.oct.ca/public/complaints-and-discipline/decisionsummary?va&"

baseHTML = urllib.urlopen(baseURL).read()

def namedCases(teacherID, thisTeacherLink):
	teacherHTML = urllib.urlopen(thisTeacherLink).read()
	teacherHTML = re.search('<div id="content" class="boxStyle">(.+?)<div class="sharing">', teacherHTML, re.S|re.DOTALL)
	teacherHTML = teacherHTML.group(1)
	teacherName = re.search('<h2>(.+?)</h2>', teacherHTML, re.S|re.DOTALL)
	teacherName = re.sub('  ', '', teacherName.group(1))
	teacherFirstName = re.search('<span id="main_0_columna_0_lblName">(.+?)<', teacherName, re.S|re.DOTALL)
	teacherFirstName = teacherFirstName.group(1)
	teacherLastName = re.search('<span style="white-space:nowrap;overflow:auto;">(.+?)<', teacherName, re.S|re.DOTALL)
	teacherLastName = teacherLastName.group(1)
	decisionsTable = re.search('<table cellspacing="0" rules="all" border="1" id="main_0_columna_0_datagridDecisions"(.+?)</table>', teacherHTML, re.S|re.DOTALL)
	for every_record in re.finditer('<tr(.+?)</tr>', decisionsTable.group(1), re.S|re.DOTALL):
		if re.search('<td class="StatusHistory" style="border-style:None;font-weight:bold;">Date</td>', every_record.group(1)):
			pass
		else: 
			Date = re.search('target="_blank">(.+?)<', every_record.group(1))
			if Date is not None:
				Date = Date.group(1)
				Type = re.search('</td><td class="StatusHistory" style="border-style:None;font-weight:normal;">(.+?)<', every_record.group(1), re.S|re.DOTALL)
				Type = Type.group(1)
				Type = re.sub('\n', '', Type)
				Type = re.sub('  ', '', Type)
				Type = re.sub('\r', '', Type)
				Committee = re.search('<td class="StatusHistory" align="right" style="border-style:None;font-weight:normal;">(.+?)<', every_record.group(1), re.S|re.DOTALL)
				Committee = re.sub('\n', '', Committee.group(1))
				Committee = re.sub('\r', '', Committee)
				Committee = re.sub('  ', '', Committee)
			else: 
				Date = "Not available"
				Type = "Not Listed"
				Committee = "Not Listed"

			caseLink = re.search('href="(.+?)"', every_record.group(1))
			caseLink = "https://www.oct.ca" + caseLink.group(1)
			teacherLink = 'https://www.oct.ca/findateacher/memberinfo?memberId=' + str(teacherID)
			teacherID = re.sub("memberID=", '', teacherID)
			rowData = [teacherID, teacherFirstName, teacherLastName, Date, Type, Committee, caseLink, teacherLink]
			print rowData
			MacFile.writerow(rowData)


def unNamedCases(teacherID, thisTeacherLink):
	teacherHTML = urllib.urlopen(thisTeacherLink).read()
	teacherHTML = re.search('<div id="content" class="boxStyle">(.+?)<div class="sharing">', teacherHTML, re.S|re.DOTALL)
	teacherHTML = teacherHTML.group(1)
	teacherName = re.search('<h2>(.+?)</h2>', teacherHTML, re.S|re.DOTALL)
	teacherName = re.sub('  ', '', teacherName.group(1))
	teacherFirstName = re.search('<span id="main_0_columna_0_lblName">(.+?)<', teacherName, re.S|re.DOTALL)
	teacherFirstName = teacherFirstName.group(1)
	decisionsTable = re.search('<table cellspacing="0" rules="all" border="1" id="main_0_columna_0_datagridDecisions"(.+?)</table>', teacherHTML, re.S|re.DOTALL)
	for every_record in re.finditer('<tr(.+?)</tr>', decisionsTable.group(1), re.S|re.DOTALL):
		if re.search('<td class="StatusHistory" style="border-style:None;font-weight:bold;">Date</td>', every_record.group(1)):
			pass
		else: 
			Date = re.search('target="_blank">(.+?)<', every_record.group(1))
			if Date is not None:
				Date = Date.group(1)
				Type = re.search('</td><td class="StatusHistory" style="border-style:None;font-weight:normal;">(.+?)<', every_record.group(1), re.S|re.DOTALL)
				Type = Type.group(1)
				Type = re.sub('\n', '', Type)
				Type = re.sub('  ', '', Type)
				Type = re.sub('\r', '', Type)
				Committee = re.search('<td class="StatusHistory" align="right" style="border-style:None;font-weight:normal;">(.+?)<', every_record.group(1), re.S|re.DOTALL)
				Committee = re.sub('\n', '', Committee.group(1))
				Committee = re.sub('\r', '', Committee)
				Committee = re.sub('  ', '', Committee)
			else: 
				Date = "Not available"
				Type = "Not Listed"
				Committee = "Not Listed"

			caseLink = re.search('href="(.+?)"', every_record.group(1))
			caseLink = "https://www.oct.ca" + caseLink.group(1)
			rowData = ["N/A", teacherFirstName, "N/A", Date, Type, Committee, caseLink, "N/A"]
			print rowData
			MacFile.writerow(rowData)


for every_teacher in re.finditer('href="/public/complaints-and-discipline/decisionsummary\?va&amp;(.+?)"', baseHTML):
	teacherID = every_teacher.group(1)
	thisTeacherLink = baseDecisionURL + teacherID
	if (re.search('memberCname', teacherID)):
		unNamedCases(teacherID, thisTeacherLink)
	if (re.search('memberID', teacherID)):
		namedCases(teacherID, thisTeacherLink)
	time.sleep(1)

